# 说明

1. 对应极客时间-小马哥-Spring 核心第 5 章：Spring IoC **依赖查找**
2. 这一章的主要学习目的是为了了解 Spring 内部如何实现功能的，换言之，这些 Spring 核心并非给终端用户使用

# 内容
1. DefaultListableBeanFactory 基本上就是 Factory 的集大成者——既可以单一类型查找，也可以集合类型，也可以层次依赖查找，用不同的方法即可

**以下是 Spring 提供的功能组成要素**

1. 单一类型依赖查找
    * 结果为一个 Bean
    * #getBean
2. 集合类型依赖查找（ObjectProviderDemo.java）
    * 结果为一个集合
    * #getBeansOfType
3. 层次性依赖查找(HierarchicalDependencyLookup.java)
    * JVM 的双亲委派类似，不过优先查找的顺序不同，JVM 中优先子容器，然后再找父容器；Spring 中优先父容器，然后再找子容器
    * 容器能查找到 Bean 分为两种，一种是自己的，一种是父容器的
    * #containsLocalBean
4. 延迟依赖查找（ObjectProviderDemo.java）
   * 这里的“延迟”并非初始化的延迟，而是说方法执行上的延迟，如果有 Bean 就执行，没有就算了
5. 依赖查找的安全性 (TypeSafetyLookupDemo.java)
   * 所谓“安全性”指的是是否抛出异常,也就是异常安全，而不是线程安全
6. 经典依赖查找的异常
   * Bean 不存在
   * Bean 不唯一

# 学习时的小开心

1. 张同学跟我分享了歌，尽管歌并不让我开心，可是他注意到了歌里的小猫叫，真的很可爱呀~总是有这样的那样的人类让我觉得人类好可爱，是值得我多分享爱的物种