package top.jolyne.thinking.in.spring.dependency.lookup;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import top.jolyne.thinking.in.spring.ioc.overview.domain.User;

/**
 * 层次性依赖查找
 *
 * @author jrl
 * @date Create in 11:43 2023/7/20
 */
public class HierarchicalDependencyLookup {
	@Bean
	public ObjectProviderDemo objectProviderDemo() {
		return new ObjectProviderDemo();
	}

	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.register(HierarchicalDependencyLookup.class);
		context.refresh();

		ConfigurableListableBeanFactory sonFactory = context.getBeanFactory();
		System.out.println("容器A：" + sonFactory);
		ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("META-INF/dependency-lookup-context.xml");
		System.out.println("当前 Bean Factory 的 Parent Bean Factory:" + sonFactory.getParentBeanFactory());
		ConfigurableListableBeanFactory fatherFactory = applicationContext.getBeanFactory();
		System.out.println("容器B：" + fatherFactory);
		// 设置 Parent BeanFactory
		sonFactory.setParentBeanFactory(fatherFactory);
		System.out.println("当前 Bean Factory 的 Parent Bean Factory:" + sonFactory.getParentBeanFactory());

		User user = sonFactory.getBean("user", User.class);
		System.out.println("子 Bean Factory直接获取 Parent Bean Factory 中的 Bean：" + user);
		// ObjectProviderDemo objectProviderDemo = fatherFactory.getBean("objectProviderDemo", ObjectProviderDemo.class);
		// System.out.println("Parent Bean Factory直接获取 子 Bean Factory中的 Bean："+objectProviderDemo);//拿不到的，跟继承的感觉一模一样
		System.out.println("子 Bean Factory 是否包含 Parent Bean Factory 中的 Bean：" + sonFactory.containsLocalBean("user"));
		System.out.println("Parent Bean Factory 是否包含 子 Bean Factory 中的 Bean：" + fatherFactory.containsLocalBean("objectProviderDemo"));
		context.close();
	}

}
