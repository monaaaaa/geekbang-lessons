package top.jolyne.thinking.in.spring.dependency.lookup;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import top.jolyne.thinking.in.spring.ioc.overview.domain.User;

/**
 * 通过 {@link ObjectProvider} 进行依赖查找，可以实现对 Bean 的延迟查找——
 * <p>
 * 延迟查找不是 Bean的延迟加载（延迟初始化），跟@Lazy是两码事，
 * ObjectProvider#getxxx 方法 底层还是通过BeanFactory来进行依赖查找的，
 * 但是在进行依赖查找前，可以制定以下规则，比如Bean找到后，再设置额外的属性，完成一些用户的自定义需求；Bean没有找到，该如何处理
 *
 * @author jrl
 * @补充 ObjectProvider 继承了 {@link ObjectFactory}
 * @date Create in 19:28 2023/7/18
 */
public class ObjectProviderDemo {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.register(ObjectProviderDemo.class);
		context.refresh();
		lookupByObjectProvider(context);
		lookupIfAvailable(context);
		lookupByStreamOps(context);
		context.close();
	}

	private static void lookupByStreamOps(AnnotationConfigApplicationContext context) {
		ObjectProvider<String> objectProvider = context.getBeanProvider(String.class);
		for (String string : objectProvider
		) {
			System.out.println(string);
		}
		objectProvider.stream().forEach(
				(string) -> {
					System.out.print("I like you " + string);
				});
	}

	private static void lookupIfAvailable(AnnotationConfigApplicationContext context) {
		ObjectProvider<User> objectProvider = context.getBeanProvider(User.class);
		User ifAvailable = objectProvider.getIfAvailable(User::getUser);
		System.out.println("延迟依赖查找的结果为：" + ifAvailable);
		// 当 Bean 被实例化了之后可以执行的操作
		objectProvider.ifAvailable((a) -> {
			System.out.println("哟呵！我接下来要消费掉你：" + a);
		});
	}

	private static void lookupByObjectProvider(AnnotationConfigApplicationContext context) {
		ObjectProvider<String> objectProvider = context.getBeanProvider(String.class);
		String s = objectProvider.getObject();
		System.out.println(s);
	}

	// DONE_Joly:怎么没看出来延迟初始化了？
	//  ---> 确实延迟初始化了，但是这和延迟依赖查找不是一个意思，延迟依赖查找的重点在有 Consumer 参数的方法，有 Bean 就干活，没有就不干
	//  “延迟依赖查找”里的“延迟”二字，确实让人觉得很有歧义。。。
	@Bean
	@Lazy
	public User user() {
		User user = new User();
		user.setName("Janine");
		user.setId(1L);
		return user;
	}

	@Bean
	@Primary
	public String helloWorld() {
		return "hello,world!";
	}

	@Bean
	public String helloFamily() {
		return "hello,family!";
	}
}
