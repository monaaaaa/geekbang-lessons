package top.jolyne.thinking.in.spring.dependency.lookup;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import top.jolyne.thinking.in.spring.ioc.overview.domain.User;

/**
 * 类型安全的异常安全依赖查找实例
 *
 * @author jrl
 * @补充 1. 异常安全——不向终端用户抛出异常
 * 2. 类型安全——和泛型做的对比，在这个demo里无需在意
 * @date Create in 12:26 2023/7/21
 */
public class TypeSafetyLookupDemo {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.register(TypeSafetyLookupDemo.class);
		context.refresh();
		// 演示 BeanFactory#getBean 的安全性--->no
		displayBeanFactoryGetBean(context);
		// 演示 ObjectFactory#getObject 的安全性--->no
		displayObjectFactoryGetBean(context);
		// 演示 ObjectProvider#getIfAvailable 的安全性--->yes
		displayObjectProviderGetIfAvailable(context);
		// 演示 ListableBeanFactory#getBeansOfType 的安全性 ---> yes
		displayListableBeanFactoryGetBeansOfType(context);
		// 演示 ObjectProvider#stream 的安全性 ---> yes
		displayObjectProviderStream(context);
		context.close();
	}

	private static void displayObjectProviderStream(AnnotationConfigApplicationContext context) {
		showException("displayObjectProviderStream", () -> {
			ObjectProvider<User> objectProvider = context.getBeanProvider(User.class);
			objectProvider.forEach(System.out::println);
		});
	}

	private static void displayListableBeanFactoryGetBeansOfType(ListableBeanFactory listableBeanFactory) {
		showException("displayListableBeanFactoryGetBeansOfType", () -> {
			listableBeanFactory.getBeansOfType(User.class);
		});
	}

	private static void displayObjectProviderGetIfAvailable(AnnotationConfigApplicationContext context) {
		showException("displayObjectProviderGetIfAvailable", () -> {
			ObjectProvider<User> objectProvider = context.getBeanProvider(User.class);
			objectProvider.getIfAvailable();
		});
	}

	private static void displayObjectFactoryGetBean(AnnotationConfigApplicationContext context) {
		showException("displayObjectFactoryGetBean", () -> {
			// ObjectProvider is ObjectFactory
			ObjectFactory<User> objectFactory = context.getBeanProvider(User.class);
			objectFactory.getObject();
		});
	}

	private static void displayBeanFactoryGetBean(BeanFactory beanFactory) {
		showException("displayBeanFactoryGetBean", () -> {
			beanFactory.getBean(User.class);
		});
	}

	private static void showException(String methodName, Runnable runnable) {
		try {
			System.out.println("==========当前方法为：" + methodName + "==========");
			runnable.run();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
