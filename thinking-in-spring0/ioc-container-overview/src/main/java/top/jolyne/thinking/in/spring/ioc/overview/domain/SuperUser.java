package top.jolyne.thinking.in.spring.ioc.overview.domain;

import top.jolyne.thinking.in.spring.ioc.overview.dependency.annotation.Super;

import javax.jws.soap.SOAPBinding;

/**
 * 超级用户
 *
 * @author jrl
 * @date Create in 13:59 2023/1/29
 */
@Super
public class SuperUser extends User {
    private String address;


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "SuperUser{" +
                "address='" + address + '\'' +
                "} " + super.toString();
    }
}
