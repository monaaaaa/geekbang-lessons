package top.jolyne.thinking.in.spring.ioc.overview.domain;

import org.springframework.core.io.Resource;
import top.jolyne.thinking.in.spring.ioc.overview.enums.City;

import java.util.Arrays;
import java.util.List;

/**
 * 用户类
 *
 * @author jrl
 * @date Create in 21:45 2023/1/28
 */
public class User {
	private Long id;
	private String name;

	// 目的：为了演示 Spring 依赖注入时进行的类型转换 (字符串->枚举)
	private City city;
	//目的：为了演示 Spring 依赖注入时进行的类型转换(字符串->类)
	private Resource configFileResource;

	// 目的：为了演示 Spring 依赖注入集合（数组）
	private City[] workCities;
	// 目的：为了演示 Spring 依赖注入集合
	private List<City> lifeCities;

	public static User getUser() {
		return new User();
	}

	public City[] getWorkCities() {
		return workCities;
	}

	public void setWorkCities(City[] workCities) {
		this.workCities = workCities;
	}

	public List<City> getLifeCities() {
		return lifeCities;
	}

	public void setLifeCities(List<City> lifeCities) {
		this.lifeCities = lifeCities;
	}

	public Resource getConfigFileResource() {
		return configFileResource;
	}

	public void setConfigFileResource(Resource configFileResource) {
		this.configFileResource = configFileResource;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "User{" +
				"id=" + id +
				", name='" + name + '\'' +
				", city=" + city +
				", configFileResource=" + configFileResource +
				", workCities=" + Arrays.toString(workCities) +
				", lifeCities=" + lifeCities +
				'}';
	}
}
