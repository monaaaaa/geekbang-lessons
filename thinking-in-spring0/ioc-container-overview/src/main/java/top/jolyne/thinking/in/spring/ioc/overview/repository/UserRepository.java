package top.jolyne.thinking.in.spring.ioc.overview.repository;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.context.ApplicationContext;
import top.jolyne.thinking.in.spring.ioc.overview.domain.User;

import java.util.Collection;

/**
 * 用户信息仓库：用于存储所有用户
 * <p>
 * 【创建目的】学习依赖注入：users 是被 Spring 通过依赖注入的方式注入到 UserRepository 实例中的
 *
 * @author jrl
 * @date Create in 16:35 2023/1/29
 */
public class UserRepository {
	// 用户 Bean
	private Collection<User> users;

	// 演示了存在非Bean的情况，BeanFactory 的实例就不是Bean，而是容器内建的依赖。
	private BeanFactory beanFactory;

	// 配合使用，演示了延迟加载
	ObjectFactory<ApplicationContext> objectFactory;

	public ObjectFactory<ApplicationContext> getObjectFactory() {
		return objectFactory;
	}

	public void setObjectFactory(ObjectFactory<ApplicationContext> objectFactory) {
		this.objectFactory = objectFactory;
	}

	public BeanFactory getBeanFactory() {
		return beanFactory;
	}

	public void setBeanFactory(BeanFactory beanFactory) {
		this.beanFactory = beanFactory;
	}

	public Collection<User> getUsers() {
		return users;
	}

	public void setUsers(Collection<User> users) {
		this.users = users;
	}
}
