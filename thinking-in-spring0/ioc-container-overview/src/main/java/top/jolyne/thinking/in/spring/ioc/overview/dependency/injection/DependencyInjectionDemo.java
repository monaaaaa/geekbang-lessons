package top.jolyne.thinking.in.spring.ioc.overview.dependency.injection;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.env.Environment;
import top.jolyne.thinking.in.spring.ioc.overview.dependency.annotation.Super;
import top.jolyne.thinking.in.spring.ioc.overview.domain.User;
import top.jolyne.thinking.in.spring.ioc.overview.repository.UserRepository;

import java.util.Collection;
import java.util.Map;

/**
 * 依赖注入
 *
 * @author jrl
 * @date Create in 21:22 2023/1/28
 */
public class DependencyInjectionDemo {
	public static void main(String[] args) throws Exception {
		// 1.. 配置 XML 配置文件
		// 2.. 启动 Spring 应用上下文
		/** knowledge point:  创建上下文*/
		BeanFactory beanFactory = new ClassPathXmlApplicationContext("META-INF/dependency-injection-context.xml");
		// 这是依赖查找
		UserRepository userRepository = (UserRepository) beanFactory.getBean("userRepository");

		// Collection<User> users = userRepository.getUsers();
		// System.out.println(users);

		// 依赖注入
		// BeanFactory beanFactory1 = userRepository.getBeanFactory();
		// System.out.println(beanFactory);
		// System.out.println(beanFactory1);
		// System.out.println(beanFactory1 == beanFactory); //false,也就是说依赖注入放进去的 BeanFactory 不是用户创建的

		// 依赖查找
		// System.out.println(beanFactory.getBean(BeanFactory.class));//NoSuchBeanDefinitionException,说明 BeanFactory 不是一个用户 Bean

		// ObjectFactory<ApplicationContext> objectFactory = userRepository.getObjectFactory();
		// System.out.println(objectFactory.getObject());
		// System.out.println(objectFactory.getObject() == beanFactory);// true


		Environment environment = beanFactory.getBean(Environment.class);// 容器自建 Bean
		System.out.println(environment);
	}
}
