package top.jolyne.thinking.in.spring.ioc.overview.container;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import top.jolyne.thinking.in.spring.ioc.overview.domain.User;

import java.util.Map;

/**
 * 【*****】
 * ApplicationContext 作为 IoC Container
 *
 * @author jrl
 * @date Create in 14:43 2023/7/7
 */
@Configuration // 不加也可以，但是加了之后可以 CGLib 提速
public class AnnotationContextAsIoCContainerDemo {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
		// 注册 Bean
		applicationContext.register(AnnotationContextAsIoCContainerDemo.class);
		// 启动应用上下文
		applicationContext.refresh();
		lookupBeansByType(applicationContext);
	}

	/**
	 * 通过 Java 注解的方式定义 Bean
	 *
	 * @author jrl
	 * @date 2023/7/7
	 */
	@Bean
	public User user() {
		return new User();
	}

	@Bean
	public User user2() {
		User user = new User();
		user.setId(2L);
		user.setName("Mona");
		return user;
	}

	private static void lookupBeansByType(BeanFactory beanFactory) {
		if (beanFactory instanceof ListableBeanFactory) {
			ListableBeanFactory listableBeanFactory = (ListableBeanFactory) beanFactory;
			Map<String, User> beans = listableBeanFactory.getBeansOfType(User.class);
			System.out.println("查到的所有的 User 集合为：" + beans);
		}
	}
}
