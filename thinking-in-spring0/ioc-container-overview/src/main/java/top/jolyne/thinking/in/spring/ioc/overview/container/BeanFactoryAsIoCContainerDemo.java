package top.jolyne.thinking.in.spring.ioc.overview.container;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import top.jolyne.thinking.in.spring.ioc.overview.domain.User;

import java.util.Map;

/**
 * BeanFactory 通过 Reader 来手动进行配置
 * @author jrl
 * @date Create in 17:06 2023/7/4
 */
public class BeanFactoryAsIoCContainerDemo {
	public static void main(String[] args) {
		DefaultListableBeanFactory defaultListableBeanFactory = new DefaultListableBeanFactory();
		XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(defaultListableBeanFactory);
		// 注册 Bean
		int beanDefinitionsNumber = reader.loadBeanDefinitions("META-INF/dependency-injection-context.xml");
		System.out.println("被发现的 BeanDefinition 数量为："+beanDefinitionsNumber);
		lookupBeansByType(defaultListableBeanFactory);
	}

	private static void lookupBeansByType(BeanFactory beanFactory) {
		if (beanFactory instanceof ListableBeanFactory) {
			ListableBeanFactory listableBeanFactory = (ListableBeanFactory) beanFactory;
			Map<String, User> beans = listableBeanFactory.getBeansOfType(User.class);
			System.out.println(beans);
		}
	}
}
