package top.jolyne.thinking.in.spring.ioc.overview.dependency.annotation;

import java.lang.annotation.*;

/**
 * 类似 Component 注解，使拿给用户来使用的
 * @author jrl
 * @date Create in 13:59 2023/1/29
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Super {

}
