package top.jolyne.thinking.in.spring.ioc.overview.dependency.lookup;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.config.ObjectFactoryCreatingFactoryBean;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import top.jolyne.thinking.in.spring.ioc.overview.dependency.annotation.Super;
import top.jolyne.thinking.in.spring.ioc.overview.domain.User;

import java.util.Map;

/**
 * 依赖查找
 *
 * @author jrl
 * @date Create in 21:22 2023/1/28
 */
public class DependencyLookupDemo {
	public static void main(String[] args) throws Exception {
		// 1.. 配置 XML 配置文件
		// 2.. 启动 Spring 应用上下文
		/** knowledge point:  创建上下文*/
		BeanFactory beanFactory = new ClassPathXmlApplicationContext("META-INF/dependency-lookup-context.xml");

		realTimeLookup(beanFactory);
		lazyTimeLookup(beanFactory);

		lookupByType(beanFactory);
		lookupCollectionByType(beanFactory);

		lookupByAnnotation(beanFactory);
	}

	/**
	 * 根据注解进行查找
	 */
	private static void lookupByAnnotation(BeanFactory beanFactory) {
		if (beanFactory instanceof ListableBeanFactory) {
			ListableBeanFactory listableBeanFactory = (ListableBeanFactory) beanFactory;
			Map<String, Object> supers = listableBeanFactory.getBeansWithAnnotation(Super.class);
			System.out.println("根据注解 @Super 进行查找:" + supers);
		}
	}

	/**
	 * 根据类型查找符合条件的多个 Bean
	 */
	private static void lookupCollectionByType(BeanFactory beanFactory) {
		if (beanFactory instanceof ListableBeanFactory) {
			ListableBeanFactory listableBeanFactory = (ListableBeanFactory) beanFactory;
			Map<String, User> userMap = listableBeanFactory.getBeansOfType(User.class);
			System.out.println("查找到的所有 User 类型 的实例集合为：" + userMap);
		}
	}

	/**
	 * 按照类型查找
	 */
	private static void lookupByType(BeanFactory beanFactory) {
		User user = beanFactory.getBean(User.class);
		System.out.println("按照类型 User 查找:" + user);
	}

	/**
	 * 延迟查找
	 * 1. 这里的延迟说的是 ：
	 * ObjectFactory 对象并没有直接返回了实际的 Bean——这里指的是 User 的 Bean，而是一个 Bean 的查找代理。
	 * 当得到 ObjectFactory 对象时，Bean 没有被创建，只有当 getObject() 方法时，才会触发 Bean 实例化等生命周期。
	 */
	private static void lazyTimeLookup(BeanFactory beanFactory) {
		// DONE_Joly:为什么拿到的是 ObjectFactory？
		//  估计创建 Bean 的时候把 id 给替换了，所以类型可以变
		ObjectFactory<User> objectFactory = (ObjectFactory<User>) beanFactory.getBean("objectFactory");
		User user1 = objectFactory.getObject();
		System.out.println("延迟查找:" + user1);
	}

	/**
	 * 实时查找
	 */
	private static void realTimeLookup(BeanFactory beanFactory) {
		User user = (User) beanFactory.getBean("user");
		System.out.println("实时查找:" + user);
	}
}
