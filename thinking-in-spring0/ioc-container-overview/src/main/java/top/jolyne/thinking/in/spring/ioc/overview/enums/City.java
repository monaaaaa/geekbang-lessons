package top.jolyne.thinking.in.spring.ioc.overview.enums;

/**
 * 城市枚举
 * TODO_Joly:枚举到底是个什么东西？是否是类似字符串？
 *
 * @author jrl
 * @date Create in 15:56 2023/8/13
 */
public enum City {
	BEIJING,
	HANGZHOU,
	SHANGHAI,
	CHENGDU,
	PARIS,
	SV
}
