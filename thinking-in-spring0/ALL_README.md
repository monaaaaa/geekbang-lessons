# 说明

1. 来自于学习极客时间的 Pony——Spring核心

# 成果说明

1. 【持久化 *****】我 配合 小马哥 的 Spring核心 写的代码
2. 【持久化 **** 】XMind:Spring 核心
3. 【内存】语雀：Spring
4. 【内存】Notability：Spring

# 重要知识点

## 1. 终端用户使用 Spring 的过程

    1. 配置元信息
    2. 注入依赖
# 生产项目
## GIS
1. 在依赖注入上，使用的大多是构造器注入的方式，采用了 lombok 提供的 @AllArgsConstructor 注解来实现的
