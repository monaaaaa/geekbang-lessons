# 目的

1. 配合小马哥《Spring 核心》第4章——Spring Bean 基础的代码

# 注意

* 概念在实现上是有交融的地方的
    * 所以无需在使用上过于纠结究竟是理论上的哪一步
    * 在使用中采取能用就行的心态，能阐述就阐述，不能就算了~
* 我的语境中的 Spring Bean 的范围是要比 BeanDefinition 要广的
    * BeanDefinition 指的是被 Spring 框架作为**解构**Spring Bean 时采用的模板

# 内容

1. 用户**定义** Spring Bean
    1. BeanDefinition 接口
        * 是 Spring Bean 的最底层体现
    2. XML
    3. 注解
        * @Bean
        * @Component
            * @Controller
            * @Service
            * @Repository
        * @Important （定义 & 注册）
2. 用户**注册** BeanDefinition （AnnotationBeanDefinitionDemo.java）
    1. Java API
        * Reader
        * Registry
    2. XML
    3. 注解
        * @Important （定义 & 注册）
3. 用户**实例化** BeanDefinition 或 Spring Bean （BeanInstantiationDemo.java）
    * 这个步骤对于用户来说应该是可以不要的，因为向 IoC 中注册了 BeanDefinition 之后就可以获得 Spring Bean，所以 Spring
      Bean 的实例化应该是在 IoC 当中有自动执行的；
    * 也就是说这一步是拿来用户要进行自定义的时候做的，当然，这本身应该也是 Spring 实现实例化的时候会使用的方法吧
    * 方式
        * 方式一：构造器（默认）
        * 方式二：静态工厂方法
        * 方式三：实例工厂方法（FactoryBean）
        * 方式四：利用 ServiceLoader 找到 FactoryBean
        * 方式五：Context#register()
        * 方式六：AutowireCapableBeanFactory#createBean()
4. 用户初始化 Spring Bean （BeanInitializationDemo.java）
    1. 延迟初始化
        * @PostConstruct
            * Java
            * Java 9 之后需要自己引入相关包
        * InitializingBean#afterProperties()
        * @Bean(initMethod = "")
            * 底层调用的 API 是 BeanDefinition#setInitMethodName
    2. 非延迟初始化 （在容器启动之前没有进行 Spring Bean 的初始化）
5. 用户销毁 Spring Bean
    * @PreDestroy
        * Java
    * DisposableBean#destroy()
    * @Bean(destroyMethod = "")
        * 底层调用的是 BeanDefinition#setDestroyMethodName(String name)
6. 用户垃圾回收 Spring Bean
    * Context#close() 会自动触发 JVM 的垃圾回收
    * System#gc() 用户可以强制执行 JVM 垃圾回收
    * Object#finalize() 用户可以调用该方法，但是这个方法不建议使用，同时也不一定会被 JVM 执行