package top.jolyne.thinking.in.spring.bean.definition;

import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.serviceloader.ServiceLoaderFactoryBean;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import top.jolyne.thinking.in.spring.bean.factory.UserFactory;
import top.jolyne.thinking.in.spring.ioc.overview.domain.User;

import java.util.ServiceLoader;

/**
 * Spring Bean 的实例化（特殊操作）
 * <p>
 * {@link ServiceLoaderFactoryBean} Spring
 * {@link ServiceLoader} Java
 *
 * @author jrl
 * @date Create in 15:16 2023/7/12
 */
public class SpecialBeanInstantiationDemo {
	public static void main(String[] args) throws Exception {
		ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("META-INF/special-bean-instantiation-context.xml");
		/* 实例化 Spring Bean 方式一：利用 ServiceLoader 来加载 BeanFactory，再利用 BeanFactory 来创建 Spring Bean */
		ServiceLoader<UserFactory> serviceLoader = applicationContext.getBean("userFactoryServiceLoader", ServiceLoader.class);
		UserFactory userFactory = serviceLoader.iterator().next();
		User user = userFactory.getUser();
		System.out.println(user);

		/* 喔！悄悄地把 bean 的 id 放在了另一个 bean 身上！ */
		ServiceLoaderFactoryBean bean = applicationContext.getBean(ServiceLoaderFactoryBean.class);
		System.out.println(bean);
		Object object = bean.getObject();
		System.out.println(object);

		/* 实例化 Spring Bean 方式二：AutowireCapableBeanFactory */
		AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
		User user1 = beanFactory.getBean("user", User.class);
		System.out.println(user1);

		User user2 = beanFactory.createBean(User.class);
		System.out.println(user2);
		System.out.println(user1 == user2);
	}
}
