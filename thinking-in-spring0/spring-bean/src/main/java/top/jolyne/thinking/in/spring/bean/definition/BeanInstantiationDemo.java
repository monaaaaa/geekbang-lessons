package top.jolyne.thinking.in.spring.bean.definition;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import top.jolyne.thinking.in.spring.ioc.overview.domain.User;

/**
 * Spring Bean 的实例化（普通操作）
 * @author jrl
 * @date Create in 20:26 2023/7/11
 */
public class BeanInstantiationDemo {
	public static void main(String[] args) {
		BeanFactory beanFactory = new ClassPathXmlApplicationContext("META-INF/bean-instantiation-context.xml");
		User beanByStaticMethodUser = beanFactory.getBean("instantiationByStaticMethod", User.class);
		System.out.println("方式一：通过静态工厂方法实例化 User："+beanByStaticMethodUser);
		User beanByInstanceMethodUser = beanFactory.getBean("bean_by_instance_method", User.class);
		System.out.println("方式二：通过实例工厂方法实例化 User："+beanByInstanceMethodUser);
		System.out.println(beanByInstanceMethodUser == beanByStaticMethodUser);
		User beanByBeanFactory = beanFactory.getBean("bean_by_bean_factory", User.class);
		System.out.println("方式三：通过 FactoryBean 实例化 User："+beanByBeanFactory);
		System.out.println(beanByBeanFactory == beanByStaticMethodUser);
	}
}
