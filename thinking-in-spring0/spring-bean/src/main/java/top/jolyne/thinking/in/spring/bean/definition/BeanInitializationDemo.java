package top.jolyne.thinking.in.spring.bean.definition;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import top.jolyne.thinking.in.spring.bean.factory.DefaultUserFactory;

/**
 * Spring Bean 初始化
 *
 * @author jrl
 * @date Create in 13:54 2023/7/15
 */
@Configuration // 表明这个类是个配置 Bean【可加可不加，主要起注释作用】
public class BeanInitializationDemo {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.register(BeanInitializationDemo.class);
		context.refresh();
		System.out.println("容器启动完成 ... ");
		DefaultUserFactory userFactory = context.getBean(DefaultUserFactory.class);
		System.out.println(userFactory);
		System.out.println("容器开始关闭 ... ");
		context.close();
		System.out.println("容器关闭完成 ... ");
	}

	/**
	 * @date 2023/7/15
	 * {@link BeanDefinition#setInitMethodName(String)} 设置初始化方法的时候，底层是用的该方法
	 */
	@Lazy // 启动延迟加载
	@Bean(initMethod = "initBean", destroyMethod = "myDestroy")
	public DefaultUserFactory defaultUserFactory() {
		System.out.println("实例化 ----- aba aba aba");
		DefaultUserFactory defaultUserFactory = new DefaultUserFactory();
		System.out.println(defaultUserFactory);
		return defaultUserFactory;
	}

}
