package top.jolyne.thinking.in.spring.bean.factory;

import top.jolyne.thinking.in.spring.ioc.overview.domain.User;

/**
 * @author jrl
 * @date Create in 13:46 2023/7/12
 */
public interface UserFactory {
	default public User getUser() {
		return User.getUser();
	}
}
