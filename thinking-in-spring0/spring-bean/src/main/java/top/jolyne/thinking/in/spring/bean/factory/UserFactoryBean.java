package top.jolyne.thinking.in.spring.bean.factory;

import org.springframework.beans.factory.FactoryBean;
import top.jolyne.thinking.in.spring.ioc.overview.domain.User;

/**
 * FactoryBean 的重点是 Bean，只不过这个 Bean 有一个重要的功能是 Factory——它是某个特定类的 Factory！
 *
 * @author jrl
 * @date Create in 14:16 2023/7/12
 */
public class UserFactoryBean implements FactoryBean {
	@Override
	public Object getObject() throws Exception {
		return User.getUser();
	}

	@Override
	public Class<?> getObjectType() {
		return User.class;
	}
}
