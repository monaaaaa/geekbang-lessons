package top.jolyne.thinking.in.spring.bean.factory;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import top.jolyne.thinking.in.spring.ioc.overview.domain.User;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @author jrl
 * @date Create in 13:47 2023/7/12
 */
public class DefaultUserFactory implements UserFactory, InitializingBean, DisposableBean {
	public User getUser() {
		User user = new User();
		user.setName("Mona");
		user.setId(1L);
		return user;
	}

	public DefaultUserFactory() {
		System.out.println("0默认构造器被调用");
	}

	/**
	 * 初始化
	 *
	 * @date 2023/7/15
	 */
	@PostConstruct // Java 的注解，JDK9+ 不在核心包中，需要用户主动导入进来
	public void init() {
		// DONE_Joly:我怎么才能拿到实例呢？
		// ---> this 呀！
		System.out.println(this);
		System.out.println("1.通过 @PostConstruct 初始化 DefaultUserFactory");
	}

	/**
	 * 初始化
	 *
	 * @date 2023/7/15
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("2.通过实现 InitializingBean#afterPropertiesSet 初始化DefaultUserFactory");
	}

	/**
	 * 初始化
	 *
	 * @date 2023/7/15
	 */
	public void initBean() {
		System.out.println("3.通过用户@Bean指定方法初始化 DefaultUserFactory");
	}

	@PreDestroy
	public void preDestroy() {
		System.out.println("1. @PreDestroy 进行 DefaultUserFactory 的销毁 ...");
	}

	@Override
	public void destroy() throws Exception {
		System.out.println("2. DisposableBean#destroy() 进行 DefaultUserFactory 的销毁 ...");
	}

	public void myDestroy() {
		System.out.println("3. @Bean(destroyMethod = '') 进行 DefaultUserFactory 的销毁 ...");
	}

}
