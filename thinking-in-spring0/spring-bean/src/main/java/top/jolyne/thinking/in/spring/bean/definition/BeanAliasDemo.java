package top.jolyne.thinking.in.spring.bean.definition;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import top.jolyne.thinking.in.spring.ioc.overview.domain.User;

/**
 * @author jrl
 * @date Create in 14:56 2023/7/9
 */
public class BeanAliasDemo {
	public static void main(String[] args) {
		BeanFactory beanFactory = new ClassPathXmlApplicationContext("META-INF/bean-definitions-context.xml");
		User monaUser = beanFactory.getBean("Mona-user", User.class);
		User user = beanFactory.getBean("user", User.class);
		System.out.println("别名 MonoUser 是否等于 User：" + (monaUser == user));
	}

	@Bean("a_user")
	public User _AnnotationBeanDefinitionDemo() {
		return new User();
	}
}
