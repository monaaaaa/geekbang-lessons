package top.jolyne.thinking.in.spring.bean.definition;

import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionReaderUtils;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import top.jolyne.thinking.in.spring.ioc.overview.domain.SuperUser;
import top.jolyne.thinking.in.spring.ioc.overview.domain.User;

import java.util.Map;

/**
 * 用注解的方式定义 Spring Bean；
 * 用三种方式向容器中【注册】 Spring Bean；
 * 在 Spring 的宏观实现思路上，总的来说就是要么用户给 Class，要么用户给 BeanDefinition
 *
 * @author jrl
 * @date Create in 16:58 2023/7/10
 */
@Import(BeanAliasDemo.class)
public class AnnotationBeanDefinitionDemo {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
		// 方式一：Java API
		applicationContext.register(AnnotationBeanDefinitionDemo.class);
		applicationContext.register(Config.class);
		applicationContext.refresh();
		System.out.println(applicationContext);
		System.out.println("----------------------");
		System.out.println("通过 API 注册本类：" + applicationContext.getBean(AnnotationBeanDefinitionDemo.class));
		System.out.println("通过注解 @Bean 定义 User Bean：" + applicationContext.getBean("user"));
		// 注册 BeanDefinition
		registryBeanDefinition(applicationContext, "user_by_beanDefinition_with_name");
		registryBeanDefinition(applicationContext, null);
		Object withName = applicationContext.getBean("user_by_beanDefinition_with_name");
		// 应该有 3 个,但实际上有很多个，看来注册的顺序和我想的不一样
		System.out.println("采用 Java API 的方式将 BeanDefinition 注册到容器中:" + withName);
		Map<String, User> userMap = applicationContext.getBeansOfType(User.class);
		System.out.println("hhhhh 猜猜现在有几个 user ：" + userMap);
		Object bean = applicationContext.getBean("top.jolyne.thinking.in.spring.ioc.overview.domain.User#0");
		System.out.println("User#0:" + bean);
		// 方式二：注解
		applicationContext.register(A.class);
		System.out.println("==============");
		System.out.println("通过 API 注册其他类A：" + applicationContext.getBean(A.class));
		System.out.println("通过注解 @Import 定义并注册 其他类BeanAliasDemo：" + applicationContext.getBean(BeanAliasDemo.class));
		System.out.println(applicationContext.getBean("a_user"));


		// 其他
		// 配置类
		Object ssssJ = applicationContext.getBean("superUser");
		System.out.println(ssssJ);


		// 显示关闭应用上下文
		applicationContext.close();
	}

	/**
	 * 采用 Java API 的方式将 BeanDefinition 注册到容器中
	 *
	 * @date 2023/7/10
	 */
	private static void registryBeanDefinition(BeanDefinitionRegistry beanDefinitionRegistry, String beanName) {
		if (StringUtils.hasText(beanName)) { // 如果 Bean 有名字，进行“命名 Bean 的注册”
			BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(User.class);
			builder.addPropertyValue("id", 1)
					.addPropertyValue("name", "Joly");
			AbstractBeanDefinition beanDefinition = builder.getBeanDefinition();
			beanDefinitionRegistry.registerBeanDefinition(beanName, beanDefinition);
		} else { // 如果 Bean 没有名字，进行“默认命名 Bean 的注册”
			BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(User.class);
			builder.addPropertyValue("id", 1)
					.addPropertyValue("name", "Joly");
			AbstractBeanDefinition beanDefinition = builder.getBeanDefinition();
			BeanDefinitionReaderUtils.registerWithGeneratedName(beanDefinition, beanDefinitionRegistry);
		}

	}

	// @Component
	private class A {

	}

	@Bean
	private User user() {
		return new User();
	}

	/**
	 * 配置类：用于进行配置的类（代替 XML）
	 *
	 * @author jrl
	 * @date 2023/7/10
	 */
	public static class Config {
		@Bean(name = {"superUser", "ssssJ"})
		public SuperUser superUser() {
			return new SuperUser();
		}
	}
}
