package top.jolyne.thinking.in.spring.bean.definition;

import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import top.jolyne.thinking.in.spring.ioc.overview.domain.User;

/**
 * 【定义】Spring Bean
 * {@link org.springframework.beans.factory.config.BeanDefinition} 构建示例
 *
 * @author jrl
 * @date Create in 13:51 2023/7/8
 */
public class BeanDefinitionCreationDemo {
	public static void main(String[] args) {
		// 一、 通过 BeanDefinitionBuilder 来构建 BeanDefinition
		// generic == 一般性的 vs root == 最顶部的
		BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(User.class);
		// 设置属性
		builder.addPropertyValue("name", "Mona")
				.addPropertyValue("id", 1);
		// 获取 BeanDefinition —— 这里只是定义和初始化，并非终态
		AbstractBeanDefinition beanDefinition = builder.getBeanDefinition();
		System.out.println(beanDefinition);

		// 二、 通过 AbstractBeanDefinition 来构建 BeanDefinition
		AbstractBeanDefinition definition = new GenericBeanDefinition();
		definition.setBeanClass(User.class);
		// 设置属性
		MutablePropertyValues mutablePropertyValues = new MutablePropertyValues();
		mutablePropertyValues.add("name","Mona")
						.add("id",1);
		definition.setPropertyValues(mutablePropertyValues);
		System.out.println(definition);
	}
}
