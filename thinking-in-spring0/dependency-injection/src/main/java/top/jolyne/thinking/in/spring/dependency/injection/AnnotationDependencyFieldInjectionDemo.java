package top.jolyne.thinking.in.spring.dependency.injection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import top.jolyne.thinking.in.spring.ioc.overview.domain.User;

import javax.annotation.Resource;

/**
 * 基于 注解 资源的采用了 字段 方式来进行依赖注入的 demo
 *
 * @author jrl
 * @knowledge 用户如何手动指定使用了 字段 方式来进行依赖注入
 * ---> （如何将 UserHolder 依赖注入到 AnnotationDependencyFieldInjectionDemo Bean 中 ）
 * 1. 注解:@Autowired、@Resource
 * @date Create in 20:04 2023/8/2
 */
public class AnnotationDependencyFieldInjectionDemo {
	/* 字段依赖注入 */
	@Autowired
	private
	// static @Autowired 会忽略静态成员变量
			UserHolder userHolder;
	@Resource
	private
	// static @Resource 会抛异常
	UserHolder userHolder2;

	public static void main(String[] args) {
		AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
		// 注册 Bean
		applicationContext.register(AnnotationDependencyFieldInjectionDemo.class);

		// 为了拿到 User bean ,因为在 XML 里面配置过 User Bean ，所以就直接把 XML 注册进去
		XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(applicationContext);
		// 注册 Bean
		reader.loadBeanDefinitions("META-INF/dependency-lookup-context.xml");

		// 启动应用上下文
		applicationContext.refresh();
		AnnotationDependencyFieldInjectionDemo demo = applicationContext.getBean(AnnotationDependencyFieldInjectionDemo.class);
		// Spring 里默认是单例模式
		System.out.println(demo.userHolder);
		System.out.println(demo.userHolder2);
		// 显示关闭应用上下文
		applicationContext.close();
	}

	/* 配置元信息 */
	@Bean
	public UserHolder userHolder(User user) {
		return new UserHolder(user);
	}
}
