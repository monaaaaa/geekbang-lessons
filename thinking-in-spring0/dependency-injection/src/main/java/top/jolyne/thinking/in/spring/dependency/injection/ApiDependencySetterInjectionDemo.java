package top.jolyne.thinking.in.spring.dependency.injection;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import top.jolyne.thinking.in.spring.ioc.overview.domain.User;

/**
 * 基于 API 资源的采用了 Setter 方式来进行依赖注入的 demo
 *
 * @author jrl
 * @knowledge 用户如何手动指定使用了 Setter 方式来进行依赖注入
 * ---> （如何将 User Bean 依赖注入到 UserHolder Bean 中 ）
 * 1. XML：<property></property>
 * 2. 注解:@Bean 本质上是构造器的方式，如果要使用 Setter，那么需要利用 Java 代码 x.setPpp(~) 的方式
 * 3. API：利用 BeanDefinition 的 API 来进行依赖注入
 * @date Create in 20:04 2023/8/2
 */
public class ApiDependencySetterInjectionDemo {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();

		// 创建 UserHolder 的 BeanDefinition
		BeanDefinition userHolderBeanDefinition = createUserHolderBeanDefinition();
		// 将 UserHolder 的 BeanDefinition 注册到应用上下文中
		applicationContext.registerBeanDefinition("userHolder", userHolderBeanDefinition);

		// 为了拿到 User bean ,因为在 XML 里面配置过 User Bean ，所以就直接把 XML 注册进去
		XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(applicationContext);
		// 注册 User bean
		reader.loadBeanDefinitions("META-INF/dependency-lookup-context.xml");

		// 启动应用上下文
		applicationContext.refresh();
		UserHolder bean = applicationContext.getBean(UserHolder.class);
		System.out.println(bean.getUser());
		// 显示关闭应用上下文
		applicationContext.close();
	}

	public static BeanDefinition createUserHolderBeanDefinition() {
		BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(UserHolder.class);
		// 这里体现了 Setter
		builder.addPropertyReference("user", "superUser");
		return builder.getBeanDefinition();
	}
}
