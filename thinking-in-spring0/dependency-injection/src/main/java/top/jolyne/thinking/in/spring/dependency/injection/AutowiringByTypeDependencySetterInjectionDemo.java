package top.jolyne.thinking.in.spring.dependency.injection;

import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;

/**
 * 采用了 Setter 方式来进行 自动绑定 的 依赖注入的 demo
 *
 * @author jrl
 * @date Create in 20:04 2023/8/2
 */
public class AutowiringByTypeDependencySetterInjectionDemo {
	public static void main(String[] args) {
		DefaultListableBeanFactory defaultListableBeanFactory = new DefaultListableBeanFactory();
		XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(defaultListableBeanFactory);
		// 注册 Bean
		reader.loadBeanDefinitions("META-INF/dependency-injection-setter.xml");
		UserHolder bean = defaultListableBeanFactory.getBean("autowiringByTypeUserHolder", UserHolder.class);
		System.out.println(bean.getUser());
	}
}
