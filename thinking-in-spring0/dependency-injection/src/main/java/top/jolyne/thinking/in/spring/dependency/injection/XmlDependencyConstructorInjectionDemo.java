package top.jolyne.thinking.in.spring.dependency.injection;

import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;

/**
 * 基于 XML 资源的采用了 构造器 方式来进行依赖注入的 demo
 *
 * @author jrl
 * @knowledge 用户如何手动指定使用了 构造器 方式来进行依赖注入
 * --->
 * 1. XML：<constructor-arg></constructor-arg>
 * @date Create in 20:04 2023/8/12
 */
public class XmlDependencyConstructorInjectionDemo {
	public static void main(String[] args) {
		DefaultListableBeanFactory defaultListableBeanFactory = new DefaultListableBeanFactory();
		XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(defaultListableBeanFactory);
		// 注册 Bean
		reader.loadBeanDefinitions("META-INF/dependency-injection-constructor.xml");
		UserHolder bean = defaultListableBeanFactory.getBean(UserHolder.class);
		System.out.println(bean.getUser());
	}
}
