package top.jolyne.thinking.in.spring.dependency.injection;

import top.jolyne.thinking.in.spring.ioc.overview.domain.User;

/**
 * {@link User}的持有类，用于演示如何向本Bean中进行依赖注入，也就是说如何将 User Bean 注入到 UserHolder 中
 *
 * @author jrl
 * @date Create in 15:46 2023/8/4
 */
public class UserHolder {
	User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UserHolder(User user) {
		this.user = user;
	}

	public UserHolder() {
	}
}
