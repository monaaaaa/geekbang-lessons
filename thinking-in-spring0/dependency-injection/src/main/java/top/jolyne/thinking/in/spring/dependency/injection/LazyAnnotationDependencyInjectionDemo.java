package top.jolyne.thinking.in.spring.dependency.injection;

import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import top.jolyne.thinking.in.spring.dependency.injection.annotation.UserGroup;
import top.jolyne.thinking.in.spring.ioc.overview.domain.User;

import java.util.Collection;

/**
 * 延迟依赖注入
 *
 * @author jrl
 * @date 2023/8/16
 */
public class LazyAnnotationDependencyInjectionDemo {


	@Autowired
	private Collection<User> allUsers;
	@Autowired
	@Qualifier("user2") // 指定 bean 的 name 或者 id
	private User user;
	@Autowired
	@Qualifier
	private Collection<User> qualifierUsers;
	@Autowired
	@UserGroup
	private Collection<User> groupUsers;
	@Autowired
	private ObjectProvider<User> userObjectProvider;

	public static void main(String[] args) {
		AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
		// 注册 Bean
		applicationContext.register(LazyAnnotationDependencyInjectionDemo.class);
		// 启动应用上下文
		applicationContext.refresh();

		LazyAnnotationDependencyInjectionDemo demo = applicationContext.getBean(LazyAnnotationDependencyInjectionDemo.class);

		User object = demo.userObjectProvider.getObject();
		System.out.println(object);
		// 显示关闭应用上下文
		applicationContext.close();
	}

	private User createUser(Long id) {
		User user = new User();
		user.setId(id);
		return user;
	}

	@Bean
	@Lazy // 这个是延迟初始化，也就是说延迟把 bean 造出来然后放到容器里
	private User user1() {
		return createUser(1L);
	}

	@Bean // 默认使用 方法名称 作为 bean 的名称
	@Primary
	private User user2() {
		return createUser(2L);
	}

	@Bean
	@Qualifier // 进行逻辑分组
	private User user3() {
		return createUser(3L);
	}

	@Bean
	@Qualifier// 进行逻辑分组
	private User user4() {
		return createUser(4L);
	}

	@Bean
	@UserGroup // 进行逻辑分组
	private User user5() {
		return createUser(5L);
	}

	@Bean
	@UserGroup// 进行逻辑分组
	private User user6() {
		return createUser(6L);
	}
}
