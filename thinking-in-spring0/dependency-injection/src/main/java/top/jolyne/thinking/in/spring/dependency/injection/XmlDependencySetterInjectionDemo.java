package top.jolyne.thinking.in.spring.dependency.injection;

import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;

/**
 * 基于 XML 资源的采用了 Setter 方式来进行依赖注入的 demo
 *
 * @author jrl
 * @knowledge 用户如何手动指定使用了 Setter 方式来进行依赖注入
 * --->
 * 1. XML：<property></property>
 * @date Create in 20:04 2023/8/2
 */
public class XmlDependencySetterInjectionDemo {
	public static void main(String[] args) {
		DefaultListableBeanFactory defaultListableBeanFactory = new DefaultListableBeanFactory();
		XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(defaultListableBeanFactory);
		// 注册 Bean
		reader.loadBeanDefinitions("META-INF/dependency-injection-setter.xml");
		UserHolder bean = defaultListableBeanFactory.getBean(UserHolder.class);
		System.out.println(bean.getUser());
	}
}
