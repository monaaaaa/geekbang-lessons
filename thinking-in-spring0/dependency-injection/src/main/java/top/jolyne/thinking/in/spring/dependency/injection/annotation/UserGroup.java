package top.jolyne.thinking.in.spring.dependency.injection.annotation;

import org.springframework.beans.factory.annotation.Qualifier;

import java.lang.annotation.*;

/**
 * 用户组注解，扩展@Qualifier
 *
 * @author jrl
 * @date Create in 19:15 2023/8/16
 */
// ！！！注解居然不是纯继承关系，不是只放上去@Qualifier就好了！！！
@Qualifier
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface UserGroup {
}
