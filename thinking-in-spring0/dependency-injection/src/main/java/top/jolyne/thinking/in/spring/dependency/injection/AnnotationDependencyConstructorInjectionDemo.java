package top.jolyne.thinking.in.spring.dependency.injection;

import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import top.jolyne.thinking.in.spring.ioc.overview.domain.User;

/**
 * 基于 注解 资源的采用了 构造器 方式来进行依赖注入的 demo
 *
 * @author jrl
 * @knowledge 用户如何手动指定使用了 构造器 方式来进行依赖注入
 * ---> （如何将 User Bean 依赖注入到 UserHolder Bean 中 ）
 * 1. XML：<constructor-arg></constructor-arg>
 * 2. 注解:@Bean 本质上是构造器的方式
 * @date Create in 20:04 2023/8/2
 */
public class AnnotationDependencyConstructorInjectionDemo {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
		// 注册 Bean
		applicationContext.register(AnnotationDependencyConstructorInjectionDemo.class);

		// 为了拿到 User bean ,因为在 XML 里面配置过 User Bean ，所以就直接把 XML 注册进去
		XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(applicationContext);
		// 注册 Bean
		reader.loadBeanDefinitions("META-INF/dependency-lookup-context.xml");

		// 启动应用上下文
		applicationContext.refresh();
		UserHolder bean = applicationContext.getBean(UserHolder.class);
		System.out.println(bean.getUser());
		// 显示关闭应用上下文
		applicationContext.close();
	}

	@Bean
	public UserHolder userHolder(User user) {
		return new UserHolder(user);
	}
}
