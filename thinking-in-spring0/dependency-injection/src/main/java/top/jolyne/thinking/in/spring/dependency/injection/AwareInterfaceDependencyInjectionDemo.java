package top.jolyne.thinking.in.spring.dependency.injection;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import top.jolyne.thinking.in.spring.ioc.overview.domain.User;

/**
 * 采用了 接口回调 方式来进行依赖注入的 demo
 *
 * @author jrl
 * @knowledge 用户如何手动指定使用了 接口回调 方式来进行依赖注入
 * --->
 * 1. 实现xxxAware接口，以此将xxx注入到本实现类中
 * @date Create in 20:04 2023/8/2
 */
public class AwareInterfaceDependencyInjectionDemo implements BeanFactoryAware, ApplicationContextAware {
	private BeanFactory beanFactory;
	private ApplicationContext applicationContext;

	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		// 注册 Bean
		context.register(AwareInterfaceDependencyInjectionDemo.class);
		// 启动应用上下文
		context.refresh();
		AwareInterfaceDependencyInjectionDemo demo = context.getBean(AwareInterfaceDependencyInjectionDemo.class);
		// true 说明是单例模式
		System.out.println(demo.beanFactory == context.getBeanFactory());
		// true  说明是单例模式
		System.out.println(context == demo.applicationContext);
		// 显示关闭应用上下文
		context.close();
	}


	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.beanFactory = beanFactory;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
