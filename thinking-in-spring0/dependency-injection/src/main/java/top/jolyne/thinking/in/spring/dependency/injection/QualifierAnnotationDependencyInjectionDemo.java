package top.jolyne.thinking.in.spring.dependency.injection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import top.jolyne.thinking.in.spring.dependency.injection.annotation.UserGroup;
import top.jolyne.thinking.in.spring.ioc.overview.domain.User;

import java.util.Collection;

/**
 * Qualifier注解进行限定功能：限定注入的bean；进行逻辑分组
 * <p>
 * 1. 用 Qualifier 怎么实现 bean 的分组？---> 单独用 Qualifier 注解，可以区分是与非；用新的注解包Qualifier，可以用来区分新的注解和其他
 * 2. 必需要在配置元信息的时候标注 Qualifier 吗？---> 不一定，分组的时候两边都要，其他时候只需要在依赖注入的时候进行（名称或id）限定
 *
 * @author jrl
 * @date Create in 18:33 2023/8/16
 * @see Qualifier
 */
public class QualifierAnnotationDependencyInjectionDemo {

	@Autowired
	@Qualifier("user2") // 指定 bean 的 name 或者 id
	private User user;
	@Autowired
	private Collection<User> allUsers;
	@Autowired
	@Qualifier
	private Collection<User> qualifierUsers;
	@Autowired
	@UserGroup
	private Collection<User> groupUsers;

	public static void main(String[] args) {
		AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
		// 注册 Bean
		applicationContext.register(QualifierAnnotationDependencyInjectionDemo.class);
		// 启动应用上下文
		applicationContext.refresh();

		QualifierAnnotationDependencyInjectionDemo demo = applicationContext.getBean(QualifierAnnotationDependencyInjectionDemo.class);
		System.out.println("查找所有的users：" + demo.allUsers);
		System.out.println("@Qualifier 用在某一个 bean上：" + demo.user);
		System.out.println("@Qualifier 用于分组：" + demo.qualifierUsers);
		System.out.println("@UserGroup 用于分组：" + demo.groupUsers);
		// 显示关闭应用上下文
		applicationContext.close();
	}

	private User createUser(Long id) {
		User user = new User();
		user.setId(id);
		return user;
	}

	@Bean
	private User user1() {
		return createUser(1L);
	}

	@Bean // 默认使用 方法名称 作为 bean 的名称
	private User user2() {
		return createUser(2L);
	}

	@Bean
	@Qualifier // 进行逻辑分组
	private User user3() {
		return createUser(3L);
	}

	@Bean
	@Qualifier// 进行逻辑分组
	private User user4() {
		return createUser(4L);
	}

	@Bean
	@UserGroup // 进行逻辑分组
	private User user5() {
		return createUser(5L);
	}

	@Bean
	@UserGroup// 进行逻辑分组
	private User user6() {
		return createUser(6L);
	}
}
