# 说明

1. 对应极客时间-小马哥-Spring 核心第 6 章：Spring IoC **依赖注入**
2. 主要看怎么用，以及在不同的用法之间如何做选择
3. 小马哥的语言能力真的不行，要包容他，但是千万不要懒惰，直接照搬他的原话，这样会显得受教育程度不高，没有文学素养，用词不严谨

# 内容

**注入和配置元信息是两个步骤，不一样的**

## 循环依赖问题 💵

1. 什么是循环依赖
    * A->B,B->A 喏，非常的循环
2. 如何解决
    * 构造器依赖注入解决不了，不过可以用ObjectProvider，在gis当中没有用ObjectProvider

## 生产项目 💰

1. 采用的方式：普遍用的构造器注入的手动方式

## 依赖注入的模式

### 概览 😎

**是 Spring 内部实现依赖注入中关于匹配实例和所需依赖时的方式**

1. 手动注入
    1. XML
    2. 注解
        * 不那么纯粹，基本上都是混合了 API 的使用
    3. API
2. 自动注入🙅
    1. 官方不推荐
    2. Autowiring

### 自动绑定（自动注入）

1. 就是开启了“预制披萨”模式，提供了 byName 或者 byType 的默认匹配规则
2. 不足：
3. 用法：XML、注解、API
4. Spring 宏观冲突选择顺序
    1. 自动绑定会被覆盖

### 依赖注入的途径 🧳

1. 构造器 😊
2. Setter
3. 字段 🙅
4. 方法
5. 接口回调

### 依赖注入类型选择 💰

![img.png](img.png)

1. 哪种依赖注入类型是最常用的？
    * 构造器依赖注入
2. 哪种依赖注入类型是最安全的？
    * 构造器依赖注入

### Setter 依赖注入

(XmlDependencySetterInjectionDemo.java etc)

**就是 Spring 利用 Setter 来进行依赖注入**
**依赖注入的方式并不那么严格的区分，在使用 Setter 的方式的时候，也会用到构造器注入，有可能也用了方法注入等**
**感受用法最重要，理论架构不需要很严谨**

1. 手动方式
    1. XML
        * <bean> <property></property></bean>
    2. 注解 （终端用户一般用的这种方式）
        * @Bean 这其实是构造器的方式，在这个方法里面再手动用 Java 代码的方式来调用 Setter
    3. API
        * BeanDefinitionBuilder#addPropertyReference(~),然后把 BeanDefinition 注册到应用上下文中
2. 自动方式（不推荐）
    1. 一般在 XML 文件里面进行配置 <bean autowire="xxx">
        1. byName
        2. byType

### 构造器 依赖注入 👍🏻

(XmlDependencyConstructorInjectionDemo.java etc)

**就是 Spring 利用构造器来进行依赖注入**

1. 手动方式
    1. XML
        * <bean> <constructor-arg></constructor-arg> </bean>
    2. 注解
    3. API
        * BeanDefinitionBuilder#addConstructorArgReference(~),然后把 BeanDefinition 注册到应用上下文中
2. 自动方式（不推荐）
    1. XML
        * <bean autowire="constructor"> </bean>
        * 查找具体的构造器入参的时候是采用的 byName 的规则来的
    2. API
        * 在 org.springframework.beans.factory.support.AbstractBeanDefinition#setAutowireMode 控制 Auto-wiring 的模式

### 实例字段依赖注入

(AnnotationDependencyFieldInjectionDemo.java)

**一般都是采用的手动的注解的方式**
** gis项目中，有时会采用这种方式 @Autowired **

1. 手动方式
    1. 注解
        1. @Autowired
            * 会忽略掉 static 的字段
            * 那么 static 字段怎么注入进来呢？
                * gis 里用的是 @PostConstruct，来对static字段进行初始化
        2. @Resource
        3. @Inject

### 方法依赖注入

(AnnotationDependencyMethodInjectionDemo.java)

**一般都是采用的手动的注解的方式**
**将方法参数注入到方法中**

1. 手动方式
    1. 注解
        1. @Autowired
        2. @Resource
        3. @Inject
        4. @Bean

### 接口回调依赖注入

**Aware接口：用于将各类xxx注入到实现类中**

## 依赖注入的数据内容

**主要感受 Spring 可以根据原配置信息来自动进行类型转换**

### 概览 😎

1. 基础类型注入
2. 集合类型注入

### 基础类型注入

**不是集合类型的就是基础类型**

![img_1.png](img_1.png)
DQ：Spring 会根据目标类把 XML 中的字符串转变为对应的数据类型？
A:没错，而且这个转换是很智能的，可以把字符串转换成枚举，也可以转换成数据类型

### 集合类型注入

![img_2.png](img_2.png)

1. 数组
2. 集合
    * Collection
        * Set
        * List
    * Map

## 依赖注入的微操

### 限定注入

(QualifierAnnotationDependencyInjectionDemo.java)
**默认之外的一些定制**
**限制体现在配置元信息和依赖注入这两个步骤中，毕竟要匹配嘛**
![img_3.png](img_3.png)

1. name or id
    * 在依赖注入的地方进行标注，用以说明想要注入的是哪个 bean
2. 分组
    * 将 @Qualifier 标注在依赖注入和配置元信息的地方，用于区分 Qualifier 和 非Qualifier
    * 用新的注解来包住Qualifier，用来区分新的注解和没有的

### 延迟注入

![img_4.png](img_4.png)
**默认是实时注入**
ObjectFactory 是 ObjectProvider 的父类
![img_5.png](img_5.png)

1. ObjectFactory
2. ObjectProvider 👍
    1. 因为异常安全

## 依赖注入的原理（部分剖析）

### 依赖注入这一步的过程 🍎

1. 找到声明
2. 找到元配置
3. 初始化bean
4. 依赖注入
   * 

### @Autowired 字段注入 or 方法注入

### @Inject （不推荐👎🏻）

### Spring 如何实现根据注解来进行依赖注入的？🧚

### 自定义依赖注入注解 🏰