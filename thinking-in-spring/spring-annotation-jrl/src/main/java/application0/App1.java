package application0;

import bean0.Family;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author jrl
 * @date Create in 20:04 2023/1/28
 */
@Component
public class App1 {
    // TODO_Joly:失败了，不是很清楚为啥
    @Autowired
    Family family;

    public App1(Family family) {
        this.family = family;
    }

    public App1() {
    }

    public void hello() {
        System.out.println(family);
    }
}
