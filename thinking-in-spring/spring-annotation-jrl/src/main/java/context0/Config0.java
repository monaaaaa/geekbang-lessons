package context0;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 这就是个容器呀！！！
 * @author jrl
 * @date Create in 19:51 2023/1/28
 */
@Configuration
@ComponentScan({"bean0","application0"})
public class Config0 {

}
