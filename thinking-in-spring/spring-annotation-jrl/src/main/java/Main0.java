import application0.App1;
import bean0.Family;
import bean0.Person;
import context0.Config0;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import sun.applet.Main;

/**
 * @author jrl
 * @date Create in 19:27 2023/1/28
 */


public class Main0 {



    public static void main(String[] args) {
        // DONE_Joly:我可以不用 XML，然后就用代码搞到一个 Spring IoC 容器吗？
        // ---> 可以的！@Configuration！
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(Config0.class);
        Object person = applicationContext.getBean("person");
        System.out.println(person);
        new App1().hello();
    }




}
