/**
 * 目的：
 * 展示了 POJO 和 Java Bean 的映射关系。
 * <p>
 * 描述人的 POJO 类。
 * 只有 getter、setter 方法的类称之为贫血模型。
 * <p>
 * 知识点：
 * 1. 在 Java Bean 中，把 setter方法叫做 writable，把 getter 方法 叫做 readable。
 * 2. 把字段叫做 property
 *
 * @author jrl
 * @date Create in 22:41 2023/1/25
 */
public class Person {
    String name;
    Integer age;

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
