import java.beans.*;
import java.util.stream.Stream;

/**
 * 目的：
 * 1. {@link java.beans.BeanInfo} 示例
 * 2. 和配置元信息相关
 * 用户添加各种各样的信息，然后在程序运行过程中，就会被 IoC框架 来运用！
 *
 * @author jrl
 * @date Create in 22:48 2023/1/25
 */
public class BeanInfoDemo {
    // DONE_Joly:Java Beans 的自省的方式？
    // ---> Introspector,这名字取的，真的很有灵性啊，自省啊，当然就把自己看的清清楚楚了！
    // 是一个获取 Java Bean 相关信息的工具类；
    // Java 自省用于 过去传统 Java GUI：比如 IDEA 工具中的字体调节等，也可以作为服务端 Bean 的描述元信息。
    public static void main(String[] args) throws IntrospectionException {
        BeanInfo beanInfo = Introspector.getBeanInfo(Person.class, Object.class);
        Stream<PropertyDescriptor> propertyDescriptors = Stream.of(beanInfo.getPropertyDescriptors());
        propertyDescriptors.forEach(
                propertyDescriptor -> {
                    System.out.println(propertyDescriptor);
                    // PropertyDescriptor 允许添加一个 属性编辑器 PropertyEditor。（这就是在配置元信息）
                    //模拟一个场景需求：
                    //GUI：text(String)->Integer
                    // 解决方案：
                    // 设置 propertyEditor：对 指定的 property 进行 类型转换
                    String name = propertyDescriptor.getName();
                    if ("age".equals(name)) {
                        propertyDescriptor.setPropertyEditorClass(StringToIntegerPropertyEditor.class);
                    }
                }
        );
    }

    static class StringToIntegerPropertyEditor extends PropertyEditorSupport {
        // TODO_Joly:没有被调用过，为啥？
        //---> 因为没有使用场景的触发吧；
        //在后面的《 Spring 数据绑定》章节会讨论
        @Override
        public void setAsText(String text) throws java.lang.IllegalArgumentException {
            setValue(Integer.valueOf(text));
            return;
        }
    }
}
