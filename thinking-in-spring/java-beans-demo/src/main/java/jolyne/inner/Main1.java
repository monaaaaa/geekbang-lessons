package jolyne.inner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author jrl
 * @date Create in 18:42 2023/1/28
 */
@Component
public class Main1 {
    @Autowired
    Person person;

    public Main1(Person person) {
        this.person = person;
    }

    public void hello(){
        System.out.println(person);
    }
}
