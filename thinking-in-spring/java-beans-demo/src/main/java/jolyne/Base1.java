package jolyne;

import jolyne.inner.Main1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import sun.applet.Main;

/**
 * @author jrl
 * @date Create in 18:29 2023/1/28
 */
@SpringBootApplication
public class Base1 {

    @Autowired
    Main1 main1;

    public Base1(Main1 main1) {
        this.main1 = main1;
    }

    public Base1() {
    }

    public static void main(String[] args) {
        SpringApplication.run(Base1.class, args);
        new Base1().main1.hello();
    }


}
